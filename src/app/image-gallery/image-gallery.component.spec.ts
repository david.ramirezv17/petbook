import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryComponent } from './image-gallery.component';
import { ImageService } from '../image.service';
import { Pipe } from '@angular/core';

class mockImageService {
  getImages() { return ['prueba1', 'prueba2']; }
  getImage(id: number) { return []; }
}
@Pipe({ name: 'filterimages' })
class mockPipe {
  transform() { return [] }
}

describe('ImageGalleryComponent', () => {
  let component: GalleryComponent;
  let fixture: ComponentFixture<GalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GalleryComponent, mockPipe],
      providers: [
        {
          provide: ImageService,
          useClass: mockImageService
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Debería crear el componente correctamente.', () => {
    expect(component).toBeTruthy();
  });

  it('Debería crear el componente con todas las imágenes del imageService.', () => {
    const allImages = component.allImages;
    expect(allImages.length).toEqual(2);
    expect(allImages).toContain('prueba1');
    expect(allImages).toContain('prueba2');
  });
});
