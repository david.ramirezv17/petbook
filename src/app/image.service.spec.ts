
import { ImageService } from './image.service';

describe('ImageService', () => {
  let service: ImageService;

  beforeEach(() => {
    service = new ImageService;
  });

  it('Debería crear el servicio', () => {
    expect(service).toBeTruthy();
  });
  describe('#getImages', () => {
    it('Debería retornar todas las imágenes', () => {
      let images = service.getImages();
      expect(images.length).toEqual(5);
    })
  })
  describe('#getImage(id)', () => {
    it('Debería retornar indefinido cuando se envía id indefinido', () => {
      let images = service.getImage(undefined);
      expect(images).toBeUndefined();
    })
    it('Debería retornar indefinido cuando se envía id no existente', () => {
      let images = service.getImage(150);
      expect(images).toBeUndefined();
    })
    it('Debería retornar la imagen de un perro cuando se envía id 1', () => {
      let images = service.getImage(1);
      expect(images.brand).toEqual('perro');
    })
  })
});
